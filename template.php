<?php

require('../admin/connect.php');

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="../sac.png" />
    <link rel="stylesheet" href="../style.css">
    <title>Document</title>
</head>
<body style="font-family: SSF4 ABUKET">

    <header>
        <div id="hIndex">
            <a href="/Ensaclopédie/index.php"><img src="../sac.png"  alt="logo " class="siteLogo"></a>
        </div>
        <img src="logo.png"  alt="logo " class="gameLogo"
        <div class="conReg">
            <a id="register" href="">Inscription</a>
            <a id="connexion" href="">Connexion</a>
        </div>



    </header>

    <main>

        <label id="searchlabel" for="searchbox">Search</label>
        <input type="text" id="searchbox" onkeyup="charSearch()" placeholder="Rechercher un personnage...">
	

        <?php
        $requete1 = $bdd->query("SELECT *
                                FROM "" 
                                ORDER BY id ASC
        ");

        echo "<div class='characterNav' id='charList'>";

            while($donnees = $requete1->fetch()){
                echo '
                    <div class="charactericon" style="background-image: url('.$donnees["img"].')">
                    <a title="' . $donnees["name"] .'" href="#'.$donnees["id"].'"> ' . $donnees["name"] . '</a> 
                        
                    </div>';
                   
            }
        echo "</div>";

        

        ?>
    </main>


    <?php
        $requete1 = $bdd->query("SELECT *
        FROM "" 
        ORDER BY id ASC
        ");


            
        while($donnees = $requete1->fetch()){
            echo '<div id="'. $donnees["id"].'" class="overlay">
                <div class="popup">
                    <h1>'.$donnees["name"].'
                    <img src="'.$donnees["command"].'"></img>
                    <a class="close" href="#">&times;</a>
                </div>
            </div>';
        }
    ?>


    <script type="text/javascript">

        function charSearch() {
            // Declare variables
            var input, filter, ul, li, a, i, txtValue;
            input = document.getElementById('searchbox');
            filter = input.value.toUpperCase();
            ul = document.getElementById("charList");
            li = ul.getElementsByTagName('div');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                txtValue = a.textContent || a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
                } else {
                li[i].style.display = "none";
                }
            }
        }

    </script>

</body>
</html>