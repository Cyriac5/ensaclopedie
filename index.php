<?php
require('admin/connect.php');
// Lancement de la session
session_start();
// Condition,si la session est détectée, on change le style 
// des boutons inscriptions et connexion
if(isset($_SESSION["username"])){
echo '<style>
        #register{
            display: none;
        }
        #connexion{
            display: none;
        }
      </style>';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="sac.png" />
    <link rel="stylesheet" href="style.css">
    <title>EnSAClopédie</title>
</head>
<body style="font-family: SSF4 ABUKET">

    <header>
        <div id="hIndex">
            <a href="index.php"><img src="sac.png"  alt="logo " class="siteLogo"></a>
        </div>
        <H1 class="gameLogo">ENSACLOPEDIE</H1>
        <div class="conReg">
            <?php
            // Condition, si la session est détectée,
            // on affiche le bouton déconnexion + le nom utilisateur
            if(isset($_SESSION["username"])){
                echo '<h5>Bienvenue, '. $_SESSION["username"] .'!</h5>
                <a id="logout" href="admin/logout.php">Déconnexion</a>
                ';
            }
            ?>
            <a id="register" href="admin/register.php">Inscription</a>
            <a id="connexion" href="admin/login.php">Connexion</a>
        </div>
    </header>

    <main>

        <label id="searchlabel" for="searchbox">Search</label>
        <input type="text" id="searchbox" onkeyup="charSearch()" placeholder="Chercher un jeu...">
	

        <?php
        $requete1 = $bdd->query("SELECT table_name 
                                FROM information_schema.tables
                                WHERE table_schema = 'ensaclopedie'
                                AND table_name != 'users';
        ");

        echo "<div class='characterNav' id='charList'>";

            while($donnees = $requete1->fetch()){
                echo '
                    <div id="gameicon" class="charactericon" style="background-image: url('.$donnees["table_name"].'/logo.png)">
                    <a title="' . $donnees["table_name"] .'" href="/Ensaclopédie/'.$donnees["table_name"].'/'.$donnees["table_name"].'.php"></a> 
                        
                    </div>';
                   
            }
        echo "</div>";

        

        ?>
    </main>


    


    <script type="text/javascript">

        function charSearch() {
            // Declare variables
            var input, filter, ul, li, a, i, txtValue;
            input = document.getElementById('searchbox');
            filter = input.value.toUpperCase();
            ul = document.getElementById("charList");
            li = ul.getElementsByTagName('div');

            // Loop through all list items, and hide those who don't match the search query
            for (i = 0; i < li.length; i++) {
                a = li[i].getElementsByTagName("a")[0];
                txtValue = a.textContent || a.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "";
                } else {
                li[i].style.display = "none";
                }
            }
        }

    </script>

</body>
</html>