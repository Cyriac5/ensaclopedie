<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="admin.css" />
  <link rel="icon" type="image/png" href="sac.png" />

</head>
<body>
<?php
// Récupérer le fichier config.php pour avoir accès à la base de données
require('config.php');
// Lancement de la session
session_start();
//Condition, on vérifie qu'il y ait bien une entrée dans le formulaire
if (isset($_POST['username'])){
  $username = stripslashes($_REQUEST['username']);
  $username = mysqli_real_escape_string($conn, $username);
  $password = stripslashes($_REQUEST['password']);
  $password = mysqli_real_escape_string($conn, $password);
  // Requête récupérant les données de la base
    $query = "SELECT * FROM `users` WHERE username='$username' and password='".hash('sha256', $password)."'";
  $result = mysqli_query($conn,$query) or die(mysql_error());
  $rows = mysqli_num_rows($result);
  if($rows==1){
      $_SESSION['username'] = $username;
      header("Location: ../index.php");
  }else{
    $message = "Le nom d'utilisateur ou le mot de passe est incorrect.";
  }
}
?>

<header>
        <div id="hIndex">
            <a href="../index.php"><img src="../sac.png"  alt="logo " class="siteLogo"></a>
        </div>
        <H1 class="gameLogo">ENSACLOPEDIE</H1>
</header>
<form class="box" action="" method="post" name="login">
<h1 class="box-title">Connexion</h1>
<input type="text" class="box-input" name="username" placeholder="Nom d'utilisateur">
<input type="password" class="box-input" name="password" placeholder="Mot de passe">
<input type="submit" value="Connexion " name="submit" class="box-button">
<p class="box-register">Vous êtes nouveau ici? <a href="register.php">S'inscrire</a></p>
<?php if (! empty($message)) { ?>
    <p class="errorMessage"><?php echo $message; ?></p>
<?php } ?>
</form>
</body>
</html>