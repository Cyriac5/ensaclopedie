-- PHP Version: 7.0.6

SET SQL_MODE + "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
-- Database: `ensaclopedie`

CREATE TABLE `artoffighting` (
    `id` int(11) NOT NULL,
    `name` varchar(100) NOT NULL,
    `img` varchar(255) NOT NULL,
    `command` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `artoffighting` (`id`, `name`, `img`, `command`
) VALUES
(1, 'Jack','imgCharacters/jack.jpg', 'command/cjack.png'),
(2, 'John', 'imgCharacters/john.jpg', 'command/cjohn.png'),
(3, 'King', 'imgCharacters/king.jpg', 'command/cking.png'),
(4, 'Lee', 'imgCharacters/lee.jpg', 'command/clee.png'),
(5, 'Mickey', 'imgCharacters/mickey.jpg', 'command/cmickey.png'),
(6, 'Mr Big', 'imgCharacters/mrbig.jpg', 'command/cmrbig.png'),
(7, 'Robert', 'imgCharacters/robert.jpg', 'command/crobert.png'),
(8, 'Ryo', 'imgCharacters/ryo.jpg', 'command/cryo.png'),
(9, 'Todoh', 'imgCharacters/todoh.jpg', 'command/ctodoh.png')
;

