-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2016 at 04:42 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE + "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
-- Database: `ensaclopedie`

CREATE TABLE `sasho` (
    `id` int(11) NOT NULL,
    `name` varchar(100) NOT NULL,
    `img` varchar(255) NOT NULL,
    `command` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




INSERT INTO `sasho` (`id`, `name`, `img`, `command`
) VALUES
(1, 'Charlotte', 'imgCharacters/charlotte.jpg','command/ccharlotte.png'),
(2, 'Earthquake', 'imgCharacters/earthquake.jpg', 'command/cearthquake.png'),
(3, 'Galford', 'imgCharacters/galford.jpg', 'command/cgalford.png'),
(4, 'Gen An', 'imgCharacters/genan.jpg', 'command/cgenan.png'),
(5, 'Hanzo', 'imgCharacters/hanzo.jpg', 'command/chanzo.png'),
(6, 'Haohmaru', 'imgCharacters/haohmaru.jpg', 'command/chaohmaru.png'),
(7, 'Jubei', 'imgCharacters/jubei.jpg', 'command/cjubei.png'),
(8, 'Kyoshiro', 'imgCharacters/kyoshiro.jpg', 'command/ckyoshiro.png'),
(9, 'Nakoruru', 'imgCharacters/nakoruru.jpg', 'command/cnakoruru.png'),
(10, 'Tam-Tam', 'imgCharacters/tamtam.jpg', 'command/ctamtam.png'),
(11, 'Ukyo', 'imgCharacters/ukyo.jpg', 'command/cukyo.png'),
(12, 'Wan Fu', 'imgCharacters/wanfu.jpg', 'command/cwanfu.png');