-- PHP Version: 7.0.6

SET SQL_MODE + "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
-- Database: `ensaclopedie`

CREATE TABLE `darkstalkers` (
    `id` int(11) NOT NULL,
    `name` varchar(100) NOT NULL,
    `img` varchar(255) NOT NULL,
    `command` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `darkstalkers` (`id`, `name`, `img`, `command`
) VALUES
(1, 'Anakaris','imgCharacters/anakaris.png', 'command/canakaris.png'),
(2, 'Bishamon', 'imgCharacters/bishamon.png', 'command/cbishamon.png'),
(3, 'Demitri', 'imgCharacters/demitri.png', 'command/cdemitri.png'),
(4, 'Felicia', 'imgCharacters/felicia.png', 'command/cfelicia.png'),
(5, 'Jon Talbain', 'imgCharacters/jontalbain.png', 'command/cjontalbain.png'),
(6, 'Lord Raptor', 'imgCharacters/lordraptor.png', 'command/clordraptor.png'),
(7, 'Morrigan', 'imgCharacters/morrigan.png', 'command/cmorrigan.png'),
(8, 'Rikuo', 'imgCharacters/rikuo.png', 'command/crikuo.png'),
(9, 'Sasquatch', 'imgCharacters/sasquatch.png', 'command/csasquatch.png'),
(10, 'Victor', 'imgCharacters/victor.png', 'command/cvictor.png')
;

